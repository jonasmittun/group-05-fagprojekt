import statistics
import math
from DataSim import *
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as st

class WillowAPI:
    global RESISTANCE_AIM, VOLTAGE
    RESISTANCE_AIM = 3.3 / 2
    VOLTAGE = 3.3

    #Placeholder method for the blackbox pseudo method
    @classmethod
    def writeI2C(cls, willow, adress: int, val):
        willow.write_to(adress, val)

    # Gets the resistor at the index (Unit: MOhm)
    @classmethod
    def get_resistor(cls, index):
        match index:
            case 0:
                return 0.001
            case 1:
                return 0.01
            case 2:
                return 0.1
            case 3:
                return 1
            case 4:
                return 10
            case 5:
                return 100
            case _:
                raise ValueError('get_resistor index out of bounds')

    # Select sensor from 8 possible by controlling Willow bits via I2C
    @classmethod
    def set_sensor(cls, willow, number: int):
        if number in range(0, 8):
            b = format(number, '03b')
            WillowAPI.writeI2C(willow, 118, b[2])
            WillowAPI.writeI2C(willow, 120, b[1])
            WillowAPI.writeI2C(willow, 122, b[0])
        else:
            raise ValueError('set_sensor index out of bounds')

    # Select resistor from 8 possible by controlling Willow bits via I2C
    @classmethod
    def set_resistor(cls, willow, number: int):
        if number in range(0, 6):
            b = format(number, '03b')
            WillowAPI.writeI2C(willow, 112, b[2])
            WillowAPI.writeI2C(willow, 114, b[1])
            WillowAPI.writeI2C(willow, 116, b[0])
        else:
            raise ValueError('set_resistor index out of bounds')

    # placeholder method for the blackbox pseudo method
    @classmethod
    def getMeasurement(cls, willow):
        return willow.get_measurement()

    @classmethod
    def get_resistance(cls, willow, sensor):
        WillowAPI.set_sensor(willow, sensor)
        differenceList = []
        for i in range(0, 6):
            resList = []
            WillowAPI.set_resistor(willow, i)
            for j in range(0, 1000):
                resList.append(abs(RESISTANCE_AIM - willow.get_measurement()))
            average = statistics.mean(resList)
            differenceList.append(average)
        minIndex = differenceList.index(min(differenceList))

        WillowAPI.set_resistor(willow, minIndex)
        measurement = willow.get_measurement()
        if measurement > 3.2:
            return -1
        resistance = (WillowAPI.get_resistor(minIndex) * (VOLTAGE - measurement)) / measurement
        return resistance

    # Please see reference sheet for a and b values :) (Looks like it's time to oil up)
    @classmethod
    def get_moist_value(cls, willow, sensor, a, b):
        r = WillowAPI.get_resistance(willow, sensor)
        if r == -1:
            return 0
        moist = (-b * math.log(10) + math.log(math.log(r) / math.log(10) + 1)) / (math.log(10) * a)
        return moist

    @classmethod
    def moisture_to_temperature_corrected_moisture(cls, moist, temperature):
        x = temperature + 2.8
        nom = moist + 0.567 - 0.0260 * x + 0.00051 * x ** 2
        denom = 0.881 * 1.0056 ** x
        print("moist at temperature {} was {} and is now {}".format(temperature, moist, nom/denom))
        return nom/denom

    @classmethod
    def get_moist_temperature_value(cls, willow, sensor, a, b, temperature):
        moist = WillowAPI.get_moist_value(willow, sensor, a, b)
        return WillowAPI.moisture_to_temperature_corrected_moisture(moist, temperature)

    @classmethod
    def get_bit(cls, number):
        if number == 1 or number == 0:
            return format(number, '01b')
        else:
            raise ValueError('get_bit passed number is not a bit')


'''
def moisture2temperature_corrected_moisture(
    u: Union[float, np.ndarray, pd.Series],
    T: Union[float, np.ndarray, pd.Series],
) -> Union[float, np.ndarray]:
    """
    Calculates temperature corrected moisture levels from the paper 'Calibrationcurves for Resistance-type moisture meters'
    by Anders Samuelsson. The formula is originally for Scotch Pine and Norway Spruce but will be applied universally
    to all moisture types.

    """
    u = np.array(u, dtype=np.float64)
    T = np.array(T, dtype=np.float64)

    x = T + 2.8
    nominator = u + 0.567 - 0.0260 * x + 0.00051 * x  2
    denominator = 0.881 * 1.0056  x

    return nominator / denominator
'''




'''
#Testing one moist simulation
willow = Willow() #creating willow simulation
willow.add_wood(Wood(-0.047, 1.079, 10), 0) #adding simulated wood to sensor 0
print("The resistance measurement is: {}\nAnd the actual reistance is: {}".format(WillowAPI.get_resistance(willow, 0), willow.Sensors[0].get_resistance()))
print("The moits measurement is: {}\nAnd the actual moist is: {}".format(WillowAPI.get_moist_value(willow, 0, -0.047, 1.079), willow.get_sensor_moist(0)))

#Quantefying moist uncertainty throug simulation
willow = Willow()
uncertainties = []
for i in range(10000):
    willow.add_wood(Wood(-0.047, 1.079, 10), 0)
    uncertainties.append(abs(WillowAPI.get_moist_value(willow, 0, -0.047, 1.079)))

plt.hist(uncertainties, bins=[9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7])
plt.show()
print("Min value: {}, Max Value: {}".format(min(uncertainties), max(uncertainties)))
print("Program ended")
'''


