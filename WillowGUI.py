#!/usr/bin/python
import csv
import datetime as dt
import time
from datetime import datetime
import tkinter
from tkinter import *
from tkinter import ttk
import numpy.ma as ma
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib import pyplot as plt, animation
from threading import Thread
from WillowAPI import *
from DataSim import *
from Wood import *

# ---- Global variables ----
global headings, pixel, img_plus, img_minus, combo_type_list, last_temp, last_humid, view, paused, run_thread, mesureingspeed
mesureingspeed = 0.5

color = ['#d31e25', '#d7a32e', '#d1c02b', '#369e4b', '#5db5b7', '#31407b', '#8a3f64', '#4f2e39']

root = Tk()
root.title("Wood moisture tracking")

# button images
pixel = PhotoImage(width=1, height=1)
img_plus = PhotoImage(file="./assets/plusimage.png")
img_minus = PhotoImage(file="./assets/minusimage.png")

play_button = PhotoImage(file="./assets/play.png")
pause_button = PhotoImage(file="./assets/pause.png")
stop_button = PhotoImage(file="./assets/stop.png")
save_button = PhotoImage(file="./assets/save.png")

last_temp = 20
last_humid = 40
def get_temperature():
    # Call to get temperature via Raspberry Pi library here
    # Until implemented we generate some faux data
    global last_temp, last_humid
    if random.random() < 0.5:
        last_temp = last_temp + 1
    else:
        last_temp = last_temp - 1

    if last_temp < -10:
        last_temp = -10

    if last_temp > 70:
        last_temp = 70

    return last_temp


def get_humidity():
    # Call to get humidity via Raspberry Pi library here
    # Until implemented we generate some faux data
    global last_temp, last_humid
    if random.random() < 0.5:
        last_humid = last_humid + 1
    else:
        last_humid = last_humid - 1

    if last_temp < 0:
        last_humid = 0

    if last_humid > 100:
        last_humid = 100

    return last_humid


# --------------- Menu Bar ---------------
def open_about():
    global window_about

    try:
        window_about.destroy()
    except(AttributeError, NameError):
        pass

    window_about = Toplevel(root)
    window_about.geometry("280x120")
    window_about.title("About")
    copypasta = Text(window_about)
    copypasta.insert(INSERT,
                     "🦍 🗣 GET IT TWISTED 🌪 , GAMBLE ✅ . PLEASE START GAMBLING 👍 . GAMBLING IS AN INVESTMENT 🎰 AND AN INVESTMENT ONLY 👍 . YOU WILL PROFIT 💰 , YOU WILL WIN ❗ ️. YOU WILL DO ALL OF THAT 💯 , YOU UNDERSTAND ⁉ ️ YOU WILL BECOME A BILLIONAIRE 💵 📈 AND REBUILD YOUR FUCKING LIFE 🤯")
    copypasta.config(state=DISABLED)
    copypasta.pack()


menubar = Menu(root)
# File
menu_file = Menu(menubar, tearoff=0)
#menu_file.add_command(label="New")
#menu_file.add_command(label="Open")
#menu_file.add_command(label="Save")
menu_file.add_separator()
menu_file.add_command(label="Exit", command=lambda: on_closing())
menubar.add_cascade(label="File", menu=menu_file)

''''# Simulate
menu_sim = Menu(menubar, tearoff=0)
menu_sim.add_command(label="Start")
menu_sim.add_command(label="Stop")
# Speed submenu
sub_menu = Menu(menu_sim, tearoff=0)
sub_menu.add_command(label='x1')
sub_menu.add_command(label='x10')
sub_menu.add_command(label='x100')
sub_menu.add_command(label='x1000')

# add the File menu to the menubar
menu_sim.add_cascade(
    label="Speed",
    menu=sub_menu
)
menubar.add_cascade(label="Simulate", menu=menu_sim)
'''

# Settings
menu_settings = Menu(menubar, tearoff=0)
menu_settings.add_command(label="Wood Manager", command=lambda: open_wood_manager())
menu_settings.add_command(label="Willow Configuration", command=lambda: open_willow_configuration())
menubar.add_cascade(label="Settings", menu=menu_settings)
# Help
menu_help = Menu(menubar, tearoff=0)
menu_help.add_command(label="Help")
menu_help.add_command(label="About", command=lambda: open_about())
menubar.add_cascade(label="Help", menu=menu_help)

root.config(menu=menubar)

# --------------- Main Frame ---------------
main = Frame(root)
main.pack()

# Sensor checkboxes
frame_checkboxes = Frame(main)
frame_checkboxes.grid(row=1, column=0)

Label(frame_checkboxes, text="Sensor:").grid(row=0)

cb_var = [tkinter.IntVar() for i in range(8)]
checkbox = [Checkbutton(frame_checkboxes, text=i + 1, variable=cb_var[i]) for i in range(8)]

for i in range(8):
    checkbox[i].grid(row=i + 1)

# ----------------------------------Data plot Figure definition setup-----------------------------
fig = plt.figure(facecolor='#F0F0F0')
ax = fig.add_subplot()
ax.format_coord = lambda x, y: ""
# set up viewing window (in this case the 25 most recent values)
ax.set_xlim([0, 2])
ax.set_ylim(0, 2)
ax.set_xlabel("Time")
ax.set_ylabel("Resistance (MΩ)")
plt.subplots_adjust(left=None, bottom=None, right=None, top=0.99)

# Specify the window as master
canvas = FigureCanvasTkAgg(fig, master=main)
canvas.draw()
canvas.get_tk_widget().grid(row=1, column=1, ipadx=40, pady=5)

# Plot Navigation toolbar
toolbarFrame = Frame(master=main)
toolbarFrame.grid(row=2, column=1)
toolbar = NavigationToolbar2Tk(canvas, toolbarFrame)
toolbar.children['!button4'].pack_forget()

# Buttons
main_button_frame = Frame(main)
main_button_frame.grid(row=5, column=1)

button_moisture = Button(main_button_frame, text="Moisture %", command=lambda: change_view(0))
button_resistance = Button(main_button_frame, text="Resistance", command=lambda: change_view(1))
button_temperature = Button(main_button_frame, text="Temperature", command=lambda: change_view(2))
button_humidity = Button(main_button_frame, text="Humidity", command=lambda: change_view(3))
button_pause_resume = Button(main_button_frame, text="Pause", command=lambda: pause_sim(button_pause_resume))

button_moisture.grid(row=0, column=0, padx=5, pady=2)
button_resistance.grid(row=0, column=1, padx=5, pady=2)
button_temperature.grid(row=0, column=2, padx=5, pady=2)
button_humidity.grid(row=0, column=3, padx=5, pady=2)
button_pause_resume.grid(row=0, column=4, padx=5, pady=2)

canvas.mpl_connect(
    "key_press_event", lambda event: print(f"you pressed {event.key}"))
canvas.mpl_connect("key_press_event", key_press_handler)

view = 0
paused = False


def change_view(i):
    global view
    view = i


def pause_sim(button):
    global paused
    if button.config('relief')[-1] == 'sunken':
        button.config(relief="raised", text="pause")
        ani.resume()
        paused = False
    else:
        button.config(relief="sunken", text="resume")
        ani.pause()
        paused = True

# ----------------------------Setting up simulation data---------------------------
# Setting up datastream
willow = Willow()

a_list = [-1.0]*8
b_list = [-1.0]*8
for i in range(8):
    willow.add_wood(Wood(a_list[i], b_list[i], 10), i)

x = [[], [], [], [], [], [], [], []]
moist = [[], [], [], [], [], [], [], []]
resistance = [[], [], [], [], [], [], [], []]
temperature = [[], [], [], [], [], [], [], []]
humidity = [[], [], [], [], [], [], [], []]
colors = [(0, 0, 1), (0, 1, 0), (0, 1, 1), (1, 0, 0), (1, 0, 1), (1, 1, 0), (0.5, 0.5, 0.5), (0, 0, 0)]
line_masks = [[], [], [], [], [], [], [], []]
line_list = []
for i in range(8):
    line, = ax.plot([], [], color=colors[i])
    line_list.append(line)
sim_running = [False]*8


def func(n):
    # Stopping animation when all are inactive
    active = False
    for i in range(8):
        if cb_var[i].get() == 1:
            active = True
    if not active:
        return line_list
    y_lim = 1
    old = datetime.now() + dt.timedelta(seconds=1)
    young = datetime.now()
    for i in range(8):
        #willow.set_sensor_moist(i, willow.get_sensor_moist(i) + 0.05)  # if it should keep updating when not active
        if cb_var[i].get() == 1 and len(moist[i]) != 0:
            line_list[i].set_xdata(x[i])
            match view:
                case 0:
                    line_list[i].set_ydata(moist[i])
                    y_lim = max(y_lim, max(moist[i]))
                    ax.set_ylabel("Moist(%)")
                case 1:
                    line_list[i].set_ydata(resistance[i])
                    y_lim = max(y_lim, max(resistance[i]))
                    ax.set_ylabel("Resistance(MΩ)")
                case 2:
                    line_list[i].set_ydata(temperature[i])
                    y_lim = max(y_lim, max(temperature[i]))
                    ax.set_ylabel("temperature(Cº)")
                case 3:
                    line_list[i].set_ydata(humidity[i])
                    y_lim = max(y_lim, max(humidity[i]))
                    ax.set_ylabel("humidity(%)")
            if len(moist[i]) != 0:
                old = max(old, max(x[i]))
                young = min(young, min(x[i]))
        else:
            line_list[i].set_xdata([])
            line_list[i].set_ydata([])
    ax.set_xlim(young, old)
    ax.set_ylim(0, y_lim + y_lim / 10)
    return line_list


ani = animation.FuncAnimation(fig, func, frames=None, interval=500, blit=False)

# ---------------- Data appending thread ----------------------
run_thread = True

def add_data():
    global mesureingspeed
    while run_thread:
        ani.pause()
        for i in range(8):
            if sim_running[i]:
                willow.set_sensor_moist(i, min(willow.get_sensor_moist(i) + 0.05, 99))
                x[i].append(datetime.now())
                resistance[i].append(WillowAPI.get_resistance(willow, i))
                temp_value = get_temperature()
                temperature[i].append(temp_value)
                if math.isnan(temp_value):
                    moist[i].append(WillowAPI.get_moist_value(willow, i, a_list[i], b_list[i]))
                else:
                    moist[i].append(WillowAPI.get_moist_temperature_value(willow, i, a_list[i], b_list[i], temp_value))
                humidity[i].append(get_humidity())
        if not paused:
            ani.resume()
        time.sleep(mesureingspeed)


data_thread = Thread(target=add_data)
data_thread.start()

# --------------- Wood Manager ---------------
def open_wood_manager():
    global window_wood_manager

    try:
        window_wood_manager.destroy()
    except (AttributeError, NameError):
        pass

    window_wood_manager = Toplevel(root)
    window_wood_manager.title("Wood Manager")

    wood_manager_frame = Frame(window_wood_manager, bg='#F0F0F0')
    wood_manager_frame.pack(padx=5, pady=5)

    global treeview

    # Buttons
    buttonframe = Frame(wood_manager_frame)
    buttonframe.pack(side=LEFT, fill=Y)
    Button(buttonframe, image=img_plus, height=20, width=20, command=lambda: open_add_wood()).pack(padx=5, pady=2)
    Button(buttonframe, image=img_minus, height=20, width=20, command=lambda: open_remove_wood(treeview)).pack(padx=5,
                                                                                                                pady=2)

    # Scrollbar
    scrollbar = Scrollbar(wood_manager_frame)
    scrollbar.pack(side=RIGHT, fill=Y)

    # Treeview
    treeview = ttk.Treeview(wood_manager_frame, yscrollcommand=scrollbar.set)
    treeview.pack()

    scrollbar.config(command=treeview.yview)

    with open("data/Tree_Table.csv", newline="") as file:
        reader = csv.reader(file)

        global headings
        headings = next(reader)

        treeview["column"] = headings
        treeview.column("#0", width=0, minwidth=0)

        for i, heading in enumerate(headings):
            treeview.heading(i, text=heading)
            treeview.column(i, width=80)

        id_list = []

        for col in reader:
            if col[0] not in id_list:
                treeview.insert('', 'end', values=col)
                id_list.append(col[0])
            else:
                print("Error: Duplicate ID: {}".format(col))

    treeview.pack()


def get_unique_id(treeview):
    id_list = [treeview.item(child)["values"][0] for child in treeview.get_children()]

    for ID in range(100000):
        if ID not in id_list:
            return ID
    return None


def write_treeview_to_csv(treeview):
    data_list = []
    for child in treeview.get_children():
        data_list.append(treeview.item(child)['values'])

    with open('data/Tree_Table.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(headings)

        # write multiple rows
        writer.writerows(data_list)


# --------------- Add Wood ---------------
def open_add_wood():
    global window_add_wood

    try:
        window_add_wood.destroy()
    except (AttributeError, NameError):
        pass

    window_add_wood = Toplevel(root)
    window_add_wood.title("Add Wood")
    window_add_wood.config(bg='#FFFFFF')

    # ---- Input Frame ----
    input_frame = Frame(window_add_wood, bg='#FFFFFF')
    input_frame.pack(side=TOP)

    # Type
    type_frame = Frame(input_frame, bg='#FFFFFF')
    type_frame.pack(side=LEFT, padx=10)

    label_type = Label(type_frame, text='Type', anchor='w', width=16, bg='#FFFFFF')
    label_type.pack(side=TOP)

    entry_border_frame = Frame(type_frame, bg='#7a7a7a')
    global entry_type
    entry_type = Entry(entry_border_frame, bd=0, textvariable=StringVar())
    entry_type.pack(ipady=1, padx=(1, 1), pady=(1, 1))
    entry_border_frame.pack()

    label_error_type = Label(type_frame, text=" ", bg='#FFFFFF', fg='#ff0000')
    label_error_type.pack(side=BOTTOM)

    # A value
    a_frame = Frame(input_frame, bg='#FFFFFF')
    a_frame.pack(side=LEFT, padx=10)

    label_a = Label(a_frame, text='A value', anchor='w', width=16, bg='#FFFFFF')
    label_a.pack(side=TOP)

    a_border_frame = Frame(a_frame, bg='#7a7a7a')
    global entry_a
    entry_a = Entry(a_border_frame, bd=0, textvariable=StringVar())
    entry_a.pack(ipady=1, padx=(1, 1), pady=(1, 1))
    a_border_frame.pack()

    label_error_a = Label(a_frame, text=" ", bg='#FFFFFF', fg='#ff0000')
    label_error_a.pack(side=BOTTOM)

    # B value
    b_frame = Frame(input_frame, bg='#FFFFFF')
    b_frame.pack(side=LEFT, padx=10)

    label_b = Label(b_frame, text='B value', anchor='w', width=16, bg='#FFFFFF')
    label_b.pack(side=TOP)

    b_border_frame = Frame(b_frame, bg='#7a7a7a')
    global entry_b
    entry_b = Entry(b_border_frame, bd=0, textvariable=StringVar())
    entry_b.pack(ipady=1, padx=(1, 1), pady=(1, 1))
    b_border_frame.pack()

    label_error_b = Label(b_frame, text=" ", bg='#FFFFFF', fg='#ff0000')
    label_error_b.pack(side=BOTTOM)

    # ---- Buttons ----
    def add_item():
        valid_type = False if "," in entry_type.get() or len(entry_type.get()) == 0 else True

        try:
            float(entry_a.get())
            valid_a = True
        except ValueError:
            valid_a = False

        try:
            float(entry_b.get())
            valid_b = True
        except ValueError:
            valid_b = False

        label_error_type["text"] = " " if valid_type else "Invalid type"
        label_error_a["text"] = " " if valid_a else "Invalid float"
        label_error_b["text"] = " " if valid_b else "Invalid float"

        if not valid_type or not valid_a or not valid_b:
            return False

        treeview.insert('', 'end', values=[get_unique_id(treeview), entry_type.get(), float(entry_a.get()), float(entry_b.get())])
        write_treeview_to_csv(treeview)
        window_add_wood.destroy()

    button_frame = Frame(window_add_wood, bg='#FFFFFF')
    button_frame.pack(side=BOTTOM)

    # Confirm
    button_confirm = Button(button_frame, text="Confirm", image=pixel, width=100, height=25, compound='center',
                            command=lambda: add_item())
    button_confirm.pack(side=LEFT, padx=10, pady=10)

    # Cancel
    button_cancel = Button(button_frame, text="Cancel", image=pixel, width=100, height=25,
                           command=lambda: window_add_wood.destroy(), compound='center')
    button_cancel.pack(side=RIGHT, padx=10, pady=10)


# --------------- Remove Wood ---------------
def open_remove_wood(treeview):
    global window_remove_wood

    if treeview.focus() == "":
        return

    try:
        window_remove_wood.destroy()
    except (AttributeError, NameError):
        pass

    window_remove_wood = Toplevel(root)
    window_remove_wood.geometry("300x100")
    window_remove_wood.title("Confirm deletion")

    # ---- Label ----
    remove_label = Label(window_remove_wood, text="Are you sure you want to delete the selected row(s)?")
    remove_label.pack(side=TOP, padx=5, pady=10)

    # ---- Buttons ----
    def remove_item():
        for i in range(len(treeview.selection())):
            treeview.delete(treeview.selection()[0])
        write_treeview_to_csv(treeview)
        window_remove_wood.destroy()

    # Confirm
    button_confirm = Button(window_remove_wood, text="Confirm", image=pixel, width=100, height=25, command=remove_item,
                            compound='center')
    button_confirm.pack(side=LEFT, padx=5, pady=10)

    # Cancel
    button_cancel = Button(window_remove_wood, text="Cancel", image=pixel, width=100, height=25,
                           command=lambda: window_remove_wood.destroy(), compound='center')
    button_cancel.pack(side=RIGHT, padx=5, pady=10)


def get_types():
    with open("data/Tree_Table.csv", newline="") as file:
        reader = csv.DictReader(file)
        type_list = []

        for row in reader:
            type_list.append(row["Type"])
        return type_list


def get_a(i):
    with open("data/Tree_Table.csv", newline="") as file:
        reader = csv.DictReader(file)
        templist = []

        for row in reader:
            templist.append(row["A value"])
        return templist[i]


def get_b(i):
    with open("data/Tree_Table.csv", newline="") as file:
        reader = csv.DictReader(file)
        templist = []

        for row in reader:
            templist.append(row["B value"])
        return templist[i]


combo_type_list = ["Select a type"] * 8


def save_data(i):
    #Setting up filename
    filename = ""
    date_format = "%Y-%m-%d-%Hh-%Mm-%Ss"

    start_time = min(x[i])
    stop_time = max(x[i])

    print(type(start_time))

    filename = filename + start_time.strftime(date_format) + "_" + stop_time.strftime(date_format) + "_sensor" + str(i) + ".csv"
    header = ['timestamp', 'temperature', 'humidity', 'ohms', 'moisture', 'type', 'a_value', 'b_value']
    #Writing data
    with open("data/" + filename, 'w', newline='') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL) #adding escape carectors (change later name input filter)
        wr.writerow(header)
        for j in range(len(x[i])):
            if moist[i][j] is ma.masked:
                continue
            try:
                wr.writerow([x[i][j], temperature[i][j], humidity[i][j], resistance[i][j], moist[i][j], combo_type_list[i], a_list[i], b_list[i]])
            except:
                print("index out of range: {}".format(i))
                return


# --------------- Willow Configuration ---------------
def open_willow_configuration():
    global window_willow_configuration

    try:
        window_willow_configuration.destroy()
    except (AttributeError, NameError):
        pass

    window_willow_configuration = Toplevel(root)
    window_willow_configuration.title("Willow Configuration")

    Label(window_willow_configuration, text="Sensor").grid(row=0, column=0)
    Label(window_willow_configuration, text="Type").grid(row=0, column=1)
    Label(window_willow_configuration, text="Alpha").grid(row=0, column=2)
    Label(window_willow_configuration, text="Beta").grid(row=0, column=3)
    Label(window_willow_configuration, text="Sim Toggle").grid(row=0, column=4)
    Label(window_willow_configuration, text="Sim Reset").grid(row=0, column=5)
    Label(window_willow_configuration, text="Sim Save").grid(row=0, column=6)

    def type_selected(event, i):
        val_index = combo_dict[i].current()
        alpha_list[i].set(get_a(val_index))
        beta_list[i].set(get_b(val_index))
        # ------------ TIE THE WOOD GENERATION AND CONNECTION TO MATPLOTLIB HERE!!! -----------------
        combo_type_list[i] = combo_dict[i]["values"][val_index]
        a_list[i] = float(get_a(val_index))
        b_list[i] = float(get_b(val_index))
        print("returned types a,b: {}, {}".format(type(a_list[i]), type(b_list[i])))
        willow.add_wood(Wood(a_list[i], b_list[i], 10), i)
        print("\"" + combo_dict[i]["values"][val_index] + "\" from sensor: " + str(i + 1))

    play_button_list = [] #for button fix
    reset_button_list = []
    save_button_list = []

    combo_dict = {}
    alpha_list = []
    beta_list = []

    for i in range(8):
        Label(window_willow_configuration, text="ID: {}".format(i + 1)).grid(row=i + 1, column=0)

        combo = ttk.Combobox(window_willow_configuration, width=18)
        combo['values'] = get_types()
        combo['state'] = 'readonly'
        combo.set(combo_type_list[i])
        combo.grid(row=i + 1, column=1)
        combo_dict[i] = combo
        combo.bind('<<ComboboxSelected>>', lambda event, i=i: type_selected(event, i))

        # Play button
        play_button_list.append(Button(window_willow_configuration, image=play_button, command=lambda i=i: toggle(play_button_list[i], i), width=20,
                                       height=20))
        play_button_list[i].grid(row=i+1, column=4)
        if sim_running[i]:
            play_button_list[i].config(relief="sunken", image=pause_button)
        # Reset button
        reset_button_list.append(Button(window_willow_configuration, text="Stop", image=stop_button,
                                        command=lambda i=i: reset_sim(i), width=20, height=20))
        reset_button_list[i].grid(row=i+1, column=5)
        # Save buttons
        save_button_list.append(Button(window_willow_configuration, image=save_button, command=lambda i=i: save_data(i),
                                       width=20,
                                       height=20))
        save_button_list[i].grid(row=i+1, column=6)

        alpha_list.append(StringVar())
        alpha_list[i].set(str(a_list[i]))
        Label(window_willow_configuration, textvariable=alpha_list[i]).grid(row=i + 1, column=2)
        beta_list.append(StringVar())
        beta_list[i].set(str(b_list[i]))
        Label(window_willow_configuration, textvariable=beta_list[i]).grid(row=i + 1, column=3)

        '''
        play_button_list.append(Button(window_willow_configuration, image=play_button, command=lambda: print("test"),
                                  width=20, height=20))
        play_button_list[i].configure(command = lambda: toggle(play_button_list[i]))
        play_button_list[i].grid(row=i + 1, column=4)
        '''

        #Button(window_willow_configuration, text="Stop", image=stop_button, width=20, height=20).grid(row=i + 1, column=5)

    def toggle(button, sensor):
        # Changing button appearances
        if button.config('relief')[-1] == 'sunken':
            button.config(relief="raised", image=play_button)
            sim_running[sensor] = False
            #masking put the pause
            moist[sensor].append(ma.masked)
            resistance[sensor].append(ma.masked)
            temperature[sensor].append(ma.masked)
            humidity[sensor].append(ma.masked)
            x[sensor].append(datetime.now())
        else:
            button.config(relief="sunken", image=pause_button)
            sim_running[sensor] = True

    def reset_sim(sensor):
        x[sensor].clear()
        moist[sensor].clear()
        resistance[sensor].clear()
        humidity[sensor].clear()
        temperature[sensor].clear()
        print("the sim was stopped: {}".format(sensor))


def on_closing():
    global run_thread
    run_thread = False
    root.destroy()


root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()

# tkinter.mainloop() #oritginal animation integration loop
