import random
import math

import numpy as np
import matplotlib.pyplot as plt


class Wood:
    def __init__(self, a, b, moist):
        self.a = a
        self.b = b
        self.moist = moist
        self.error = np.random.normal(0.0, 0.8, None)
        while abs(self.error) > 2 * 0.8:
            self.error = np.random.normal(0.0, 0.8, None)

    def get_resistance(self):
        if self.moist < 0:
            raise ValueError('negative moist is not possible')
        # Maybe implement temperature later
        return pow(10, pow(10, self.a * (self.moist + self.error / (1 + self.moist * 0.15)) + self.b) - 1)

    def set_moist(self, moist):
        self.moist = moist

    #Testing purposes only
    def get_moist(self):
        return self.moist

    #Testing purposes only
    def get_unsertain_moist(self):
        return self.moist+self.error


# Testing and plotting the results of wood resistance
'''
test = Wood(-0.047, 1.079)
x = []
y = []
for i in range(100):
    xi = np.random.normal(9.8, 0.4, None)
    x.append(xi)
    y.append(test.get_resistance(xi))
for i in range(100):
    xi = np.random.normal(14, 0.4, None)
    x.append(xi)
    y.append(test.get_resistance(xi))
for i in range(100):
    xi = np.random.normal(17, 0.4, None)
    x.append(xi)
    y.append(test.get_resistance(xi))

plt.rcParams["figure.figsize"] = (12, 6)
plt.xlim(0, 30)
plt.ylim(0.1, 100000)
plt.yscale("log")
plt.xscale("linear")
plt.scatter(x, y, 0.5)
plt.title("Generated Resistances for oak from Central Europe")
plt.xlabel("actual moist content \%")
plt.ylabel("generated resistance MOhm")
plt.show()
'''
