from unittest import TestCase
from DataSim import *
from WillowAPI import *

class TestWillowAPI(TestCase):
    def test_write_i2c(self):
        willow = Willow()
        WillowAPI.writeI2C(willow, 112, WillowAPI.get_bit(1))
        WillowAPI.writeI2C(willow, 116, WillowAPI.get_bit(1))
        self.assertEqual(willow.get_resistance_number(), 5)
        self.assertEqual(willow.get_sensor_number(), 0)
        WillowAPI.writeI2C(willow, 120, WillowAPI.get_bit(1))
        self.assertEqual(2, willow.get_sensor_number())

    def test_set_sensor(self):
        willow = Willow()
        WillowAPI.set_sensor(willow, 3)
        self.assertEqual(willow.get_sensor_number(), 3)
        WillowAPI.set_sensor(willow, 7)
        self.assertEqual(willow.get_sensor_number(), 7)
        with self.assertRaises(Exception):
            WillowAPI.set_sensor(willow, 20)

    def test_set_resistor(self):
        willow = Willow()
        WillowAPI.set_resistor(willow, 3)
        self.assertEqual(willow.get_resistance_number(), 3)
        WillowAPI.set_resistor(willow, 5)
        self.assertEqual(willow.get_resistance_number(), 5)
        with self.assertRaises(Exception):
            WillowAPI.set_resistor(willow, 6)

    def test_get_resistance(self):
        willow = Willow()
        willow.add_wood(Wood(-0.047, 1.079, 10), 3)
        self.assertTrue(500 < WillowAPI.get_resistance(willow, 3) < 9000)
        willow.set_sensor_moist(3, 15)
        self.assertTrue(10 < WillowAPI.get_resistance(willow, 3) < 500)
        willow.set_sensor_moist(3, 18)
        self.assertTrue(1 < WillowAPI.get_resistance(willow, 3) < 50)
        with self.assertRaises(Exception):
            WillowAPI.get_resistance(willow, 12)
        self.assertEqual(-1, WillowAPI.get_resistance(willow, 0))

    def test_get_moist_value(self):
        willow = Willow()
        willow.add_wood(Wood(-0.047, 1.079, 10), 3)
        self.assertTrue(9.3 < WillowAPI.get_moist_value(willow, 3, -0.047, 1.07) < 10.7)
        willow.set_sensor_moist(3, 15)
        self.assertTrue(13.3 < WillowAPI.get_moist_value(willow, 3, -0.047, 1.07) < 16.7)
        willow.set_sensor_moist(3, 18)
        self.assertTrue(17.3 < WillowAPI.get_moist_value(willow, 3, -0.047, 1.07) < 19.7)
        self.assertEqual(WillowAPI.get_moist_value(willow, 0, -0.047, 1.07), 0)

    def test_moisture_to_temperature_corrected_moisture(self):
        willow = Willow()
        willow.add_wood(Wood(-0.047, 1.079, 20), 3)
        moist = WillowAPI.get_moist_value(willow, 3, -0.047, 1.079)
        corrected = WillowAPI.moisture_to_temperature_corrected_moisture(moist, 20)
        self.assertTrue(9.3 < corrected < 10.7)