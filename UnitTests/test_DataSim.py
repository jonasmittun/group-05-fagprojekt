from unittest import TestCase
from DataSim import *
from WillowAPI import *

class TestWillow(TestCase):
    def test_get_measurement(self):
        test = Willow()
        test.add_wood(Wood(-0.047, 1.079, 14), 0)
        #ToDo: Change to use the algorithm for all willow resistaces
        test.write_to(112, WillowAPI.get_bit(1))
        test.write_to(116, WillowAPI.get_bit(1))
        self.assertTrue(0 < test.get_measurement() < 3.3)

    def test_get_resistance_number(self):
        willow = Willow()
        willow.write_to(112, WillowAPI.get_bit(1))
        self.assertTrue(willow.Resistor_Bank[willow.get_resistance_number()] == 0.01)
        willow = Willow()
        willow.write_to(114, WillowAPI.get_bit(1))
        self.assertTrue(willow.Resistor_Bank[willow.get_resistance_number()] == 0.1)
        willow = Willow()
        willow.write_to(116, WillowAPI.get_bit(1))
        self.assertTrue(willow.Resistor_Bank[willow.get_resistance_number()] == 10)
        willow = Willow()
        willow.write_to(124, WillowAPI.get_bit(1))
        willow.write_to(126, WillowAPI.get_bit(1))
        willow.write_to(118, WillowAPI.get_bit(1))
        willow.write_to(120, WillowAPI.get_bit(1))
        willow.write_to(122, WillowAPI.get_bit(1))
        self.assertTrue(willow.get_resistance_number() == 0)

    def test_get_sensor_number(self):
        willow = Willow()
        willow.write_to(118, WillowAPI.get_bit(1))
        self.assertTrue(willow.get_sensor_number() == 1)
        willow = Willow()
        willow.write_to(120, WillowAPI.get_bit(1))
        self.assertTrue(willow.get_sensor_number() == 2)
        willow = Willow()
        willow.write_to(122, WillowAPI.get_bit(1))
        self.assertTrue(willow.get_sensor_number() == 4)
        willow = Willow()
        willow.write_to(124, WillowAPI.get_bit(1))
        willow.write_to(126, WillowAPI.get_bit(1))
        willow.write_to(112, WillowAPI.get_bit(1))
        willow.write_to(114, WillowAPI.get_bit(1))
        willow.write_to(116, WillowAPI.get_bit(1))
        self.assertTrue(willow.get_sensor_number() == 0)

    def test_set_sensor_moist(self):
        test = Willow()
        test.add_wood(Wood(-0.047, 1.079, 14), 0)
        self.assertEqual(test.get_sensor_moist(0), 14)
        test.set_sensor_moist(0, 10)
        self.assertEqual(test.get_sensor_moist(0), 10)
        with self.assertRaises(Exception):
            test.set_sensor_moist(0, -5)
        with self.assertRaises(Exception):
            test.set_sensor_moist(0, 298)
