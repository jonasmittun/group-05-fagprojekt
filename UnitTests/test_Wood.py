from unittest import TestCase
from Wood import *


class TestWood(TestCase):
    def test_get_resistance(self):
        wood = Wood(-0.047, 1.079, 10)
        self.assertIsInstance(wood, Wood)
        self.assertTrue(500 < wood.get_resistance() < 9000)
        wood.set_moist(15)
        self.assertTrue(10 < wood.get_resistance() < 500)
        wood.set_moist(18)
        self.assertTrue(1 < wood.get_resistance() < 50)
        for i in range(10000):
            wood = Wood(-0.047, 1.079, 10)
            for j in range(10):
                wood.set_moist(j*5)
                self.assertTrue(wood.get_resistance() > 0)
        with self.assertRaises(Exception):
            wood.set_moist(-5)
            wood.get_resistance()
