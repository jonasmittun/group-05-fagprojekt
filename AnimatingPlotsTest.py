#!/usr/bin/python
import tkinter
from tkinter import *
from tkinter import ttk
import sys
# from pandas import DataFrame
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import datetime
import random
import numpy as np
global pixel, img_plus, img_minus
from WillowAPI import *
from DataSim import *
from Wood import *
import tkinter
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib import pyplot as plt, animation
import numpy as np

#plt.rcParams["figure.figsize"] = [7.00, 3.50]
#plt.rcParams["figure.autolayout"] = True

root = Tk()
root.title("Wood moister tracking")

# button images
pixel = PhotoImage(width=1, height=1)
img_plus = PhotoImage(file="./assets/plusimage.png")
img_minus = PhotoImage(file="./assets/minusimage.png")


# --------------- Menu Bar ---------------
def open_about():
    global window_about

    try:
        window_about.destroy()
    except(AttributeError, NameError):
        pass

    window_about = Toplevel(root)
    window_about.geometry("280x120")
    window_about.title("About")
    copypasta = Text(window_about)
    copypasta.insert(INSERT,
                     "🦍 🗣 GET IT TWISTED 🌪 , GAMBLE ✅ . PLEASE START GAMBLING 👍 . GAMBLING IS AN INVESTMENT 🎰 AND AN INVESTMENT ONLY 👍 . YOU WILL PROFIT 💰 , YOU WILL WIN ❗ ️. YOU WILL DO ALL OF THAT 💯 , YOU UNDERSTAND ⁉ ️ YOU WILL BECOME A BILLIONAIRE 💵 📈 AND REBUILD YOUR FUCKING LIFE 🤯")
    copypasta.config(state=DISABLED)
    copypasta.pack()


menubar = Menu(root)
# File
menu_file = Menu(menubar, tearoff=0)
menu_file.add_command(label="New")
menu_file.add_command(label="Open")
menu_file.add_command(label="Save")
menu_file.add_separator()
menu_file.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=menu_file)
# Simulate
menu_sim = Menu(menubar, tearoff=0)
menu_sim.add_command(label="Start")
menu_sim.add_command(label="Stop")
# Speed submenu
sub_menu = Menu(menu_sim, tearoff=0)
sub_menu.add_command(label='x1')
sub_menu.add_command(label='x10')
sub_menu.add_command(label='x100')
sub_menu.add_command(label='x1000')

# add the File menu to the menubar
menu_sim.add_cascade(
    label="Speed",
    menu=sub_menu
)
menubar.add_cascade(label="Simulate", menu=menu_sim)

# Settings
menu_settings = Menu(menubar, tearoff=0)
menu_settings.add_command(label="Wood Manager", command=lambda: open_wood_manager())
menu_settings.add_command(label="Willow Configuration", command=lambda: open_willow_configuration())
menubar.add_cascade(label="Settings", menu=menu_settings)
# Help
menu_help = Menu(menubar, tearoff=0)
menu_help.add_command(label="Help")
menu_help.add_command(label="About", command=lambda: open_about())
menubar.add_cascade(label="Help", menu=menu_help)

root.config(menu=menubar)

# --------------- Main Frame ---------------
main = Frame(root)
main.pack()

# Sensor checkboxes
frame_checkboxes = Frame(main)
frame_checkboxes.grid(row=1, column=0)

Label(frame_checkboxes, text="Sensor:").grid(row=0)

cb_var = [tkinter.IntVar() for i in range(8)]

def on_check():
    print("toggling box on: {},{},{},{},{},{},{},{}".format(cb_var[0].get(),cb_var[1].get(),cb_var[2].get(),cb_var[3].get(),cb_var[4].get(),cb_var[5].get(),cb_var[6].get(),cb_var[7].get()))

checkbox = [Checkbutton(frame_checkboxes, text=i + 1, variable=cb_var[i], command=on_check) for i in range(8)]

for i in range(8):
    checkbox[i].grid(row=i + 1)

#----------------------------------Data plot Figure definition setup-----------------------------
fig = plt.figure()
ax = fig.add_subplot()
#set up viewing window (in this case the 25 most recent values)
ax.set_xlim([0, 2])
ax.set_ylim(0, 2)

# Specify the window as master
canvas = FigureCanvasTkAgg(fig, master=main)
canvas.draw()
canvas.get_tk_widget().grid(row=1, column=1, ipadx=40, ipady=20)

# Plot Navigation toolbar
toolbarFrame = Frame(master=main)
toolbarFrame.grid(row=2, column=1)
toolbar = NavigationToolbar2Tk(canvas, toolbarFrame)
toolbar.children['!button4'].pack_forget()

# Buttons
button_moisture = Button(main, text="Moisture %")
button_resistance = Button(main, text="Resistance")
button_temperature = Button(main, text="Temperature")

button_moisture.grid(row=5, column=0)
button_resistance.grid(row=5, column=1)
button_temperature.grid(row=5, column=2)

canvas.mpl_connect(
    "key_press_event", lambda event: print(f"you pressed {event.key}"))
canvas.mpl_connect("key_press_event", key_press_handler)

#----------------------------Setting up simulation data---------------------------
#test on click
global stop
stop = False
def onclick(event):
    global stop
    if stop:
        ani.pause()
    else:
        ani.resume()
    stop = not stop

cid = fig.canvas.mpl_connect('button_press_event', onclick)

#setting up datastream
willow = Willow()
a_list = [-0.038, -0.042, -0.045, -0.047, -0.045, -0.034, -0.040, -0.044]
b_list = [1.052, 1.112, 1.147, 1.081, 1.116, 1.014, 1.055, 1.131]
for i in range(8):
    willow.add_wood(Wood(a_list[i], b_list[i], 10), i)

x = [[], [], [], [], [], [], [], []]
moist = [[], [], [], [], [], [], [], []]
resistance = [[], [], [], [], [], [], [], []]
line0, = ax.plot([], [], color=(0, 0, 1))
line1, = ax.plot([], [], color=(0, 1, 0))
line2, = ax.plot([], [], color=(0, 1, 1))
line3, = ax.plot([], [], color=(1, 0, 0))
line4, = ax.plot([], [], color=(1, 0, 1))
line5, = ax.plot([], [], color=(1, 1, 0))
line6, = ax.plot([], [], color=(0.5, 0.5, 0.5))
line7, = ax.plot([], [], color=(0, 0, 0))
line_list = [line0, line1, line2, line3, line4, line5, line6, line7]

def func(n):
    #print("box checked: {}".format(1 in cb_var))
    y_lim = 1
    x_lim = 1
    for i in range(8):
        willow.set_sensor_moist(i, willow.get_sensor_moist(i) + 0.05) #if it should keep updating when not active
        moist[i].append(WillowAPI.get_resistance(willow, i)) #if it should keep updating when not active
        x[i].append(n + 1) #for updating when not active
        if cb_var[i].get() == 1:
            #willow.set_sensor_moist(i, willow.get_sensor_moist(i) + 0.05) #outcomment when updating while not active
            #x[i].append(n + 1) #outcomment when updating while not active
            line_list[i].set_xdata(x[i])
            #moist[i].append(WillowAPI.get_resistance(willow, i)) #outcomment when updating while not active
            line_list[i].set_ydata(moist[i])
            y_lim = max(y_lim, max(moist[i]))
            x_lim = max(x_lim, max(x[i]))
        else:
            line_list[i].set_xdata([])
            line_list[i].set_ydata([])
    ax.set_xlim(0, x_lim)
    ax.set_ylim(0, y_lim + y_lim/10)
    return line_list

ani = animation.FuncAnimation(fig, func, frames=None, interval=30, blit=False)

# --------------- Wood Manager ---------------
def open_wood_manager():
    global window_wood_manager

    try:
        window_wood_manager.destroy()
    except (AttributeError, NameError):
        pass

    window_wood_manager = Toplevel(root, bg='#FFFFFF')
    window_wood_manager.title("Wood Manager")

    wood_manager_frame = Frame(window_wood_manager, bg='#FFFFFF')
    wood_manager_frame.pack(padx=5, pady=5)

    global treeview

    # Buttons
    buttonframe = Frame(wood_manager_frame, bg='#FFFFFF')
    buttonframe.pack(side=LEFT, anchor=NW)
    buttonframe1 = Frame(buttonframe, bg='#FFFFFF')
    buttonframe1.pack()
    Button(buttonframe1, image=img_plus, height=20, width=20, command=lambda: open_add_wood()).pack(padx=5, pady=2, fill=BOTH)
    buttonframe2 = Frame(buttonframe, bg='#FFFFFF')
    buttonframe2.pack()
    Button(buttonframe2, image=img_minus, height=20, width=20, command=lambda: open_remove_wood(treeview)).pack(padx=5, pady=2, fill=BOTH)

    # Scrollbar
    scrollbar = Scrollbar(wood_manager_frame)
    scrollbar.pack(side=RIGHT, fill=Y)

    # Treeview
    treeview = ttk.Treeview(wood_manager_frame, yscrollcommand=scrollbar.set)
    treeview.config(columns=('wood_id', 'wood_name', 'wood_type', 'wood_a', 'wood_b'))
    treeview.pack()

    scrollbar.config(command=treeview.yview)

    # Format Columns
    treeview.column("#0", width=0, stretch=NO)
    treeview.column("wood_id", anchor=CENTER, width=80)
    treeview.column("wood_name", anchor=CENTER, width=80)
    treeview.column("wood_type", anchor=CENTER, width=80)
    treeview.column("wood_a", anchor=CENTER, width=80)
    treeview.column("wood_b", anchor=CENTER, width=80)

    # Create Headings
    treeview.heading("#0", text="", anchor=CENTER)
    treeview.heading("wood_id", text="ID", anchor=CENTER)
    treeview.heading("wood_name", text="Name", anchor=CENTER)
    treeview.heading("wood_type", text="Type", anchor=CENTER)
    treeview.heading("wood_a", text="a value", anchor=CENTER)
    treeview.heading("wood_b", text="b value", anchor=CENTER)

    # Add data
    treeview.insert(parent='', index=69, values=('1', 'Stykke fra Ikea stol', 'Svensk Eeg', '9001', '69'))
    treeview.pack()

# --------------- Add Wood ---------------
def open_add_wood():
    global window_add_wood

    try:
        window_add_wood.destroy()
    except (AttributeError, NameError):
        pass

    window_add_wood = Toplevel(root)
    window_add_wood.title("Add Wood")

    window_add_wood.columnconfigure(0, weight=1)
    window_add_wood.columnconfigure(1, weight=1)
    window_add_wood.columnconfigure(2, weight=1)
    window_add_wood.columnconfigure(3, weight=1)

    # ---- Input ----
    # Name
    label_name = Label(window_add_wood, text='Name')
    label_name.grid(row=0, column=0)

    frame_name = Frame(window_add_wood, bg='#7a7a7a')
    Entry(frame_name, bd=0, textvariable=StringVar()).pack(ipady=1, padx=(1, 1), pady=(1, 1))
    frame_name.grid(row=1, column=0, padx=10)

    # Type
    label_type = Label(window_add_wood, text='Type', anchor='w')
    label_type.grid(row=0, column=1)

    combo = ttk.Combobox(window_add_wood, textvariable=StringVar(), width=18)
    combo.config(values=('Bahamas', 'Canada', 'Cuba', 'United States'))
    combo.grid(row=1, column=1, padx=10)

    # A Value
    label_a = Label(window_add_wood, text='A Value', anchor='w')
    label_a.grid(row=0, column=2)

    frame_a = Frame(window_add_wood, bg='#7a7a7a')
    Entry(frame_a, bd=0, textvariable=StringVar(), width=20).pack(ipady=1, padx=(1, 1), pady=(1, 1))
    frame_a.grid(row=1, column=2, padx=10)

    # B Value
    label_b = Label(window_add_wood, text='B Value', anchor='w')
    label_b.grid(row=0, column=3)

    frame_b = Frame(window_add_wood, bg='#7a7a7a')
    Entry(frame_b, bd=0, textvariable=StringVar()).pack(ipady=1, padx=(1, 1), pady=(1, 1))
    frame_b.grid(row=1, column=3, padx=10)

    # Margin
    frame_margin = Frame(window_add_wood)
    frame_margin.grid(row=4, column=2, ipady=10)

    # ---- Buttons ----
    # Confirm
    button_confirm = Button(window_add_wood, text="Confirm", image=pixel, width=100, height=25, compound='center')
    button_confirm.grid(row=5, column=1, pady=10)

    # Cancel
    button_cancel = Button(window_add_wood, text="Cancel", image=pixel, width=100, height=25, command=lambda: window_add_wood.destroy(), compound='center')
    button_cancel.grid(row=5, column=2, pady=10)


# --------------- Remove Wood ---------------
def open_remove_wood(treeview):
    global window_remove_wood

    if treeview.focus() == "":
        return

    try:
        window_remove_wood.destroy()
    except (AttributeError, NameError):
        pass

    window_remove_wood = Toplevel(root)
    window_remove_wood.geometry("220x80")
    window_remove_wood.title("Confirm deletion")

    # ---- Label ----
    remove_label = Label(window_remove_wood, text="Are you sure you want to delete xxx ?")
    remove_label.pack(side=TOP, padx=5, pady=10)

    # ---- Buttons ----
    def remove_item():
        for i in range(len(treeview.selection())):
            treeview.delete(treeview.selection()[0])
        window_remove_wood.destroy()

    # Confirm
    button_confirm = Button(window_remove_wood, text="Confirm", image=pixel, width=100, height=25, command=remove_item,
                            compound='center')
    button_confirm.pack(side=LEFT, padx=5, pady=10)

    # Cancel
    button_cancel = Button(window_remove_wood, text="Cancel", image=pixel, width=100, height=25,
                           command=lambda: window_remove_wood.destroy(), compound='center')
    button_cancel.pack(side=RIGHT, padx=5, pady=10)

# --------------- Willow Configuration ---------------
def open_willow_configuration():
    global window_willow_configuration

    try:
        window_willow_configuration.destroy()
    except (AttributeError, NameError):
        pass

    window_willow_configuration = Toplevel(root)
    window_willow_configuration.title("Willow Configuration")


tkinter.mainloop()