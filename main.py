import RPi.GPIO as GPIO
from time import sleep

print("GPIO Version: %s" % GPIO.VERSION)

pin_sda = 2
pin_scl = 3

pin_dht22 = 27

# Setup Pins
GPIO.setmode(GPIO.BCM)
#GPIO.setup(pin_sda, GPIO.IN)
#GPIO.setup(pin_scl, GPIO.IN)

GPIO.setup(pin_dht22, GPIO.IN)

try:
	while True:
		#sda = GPIO.input(pin_sda)
		#scl = GPIO.input(pin_scl)
		#print("SDA: %d\nSCL: %d" % (sda, scl))
		read = GPIO.input(pin_dht22)
		print("Readout: %f" % (read))
		sleep(1)
except KeyboardInterrupt:
	GPIO.cleanup()
