# Willow functional simulator
from Wood import *
# I2C functionality

class Willow:
    def __init__(self):
        self.PCF_REGISTER = [format(0, '01b')] * 8
        self.Sensors = [None] * 8
        self.Resistor_Bank = [0.001, 0.01, 0.1, 1, 10, 100]
        self.power = 3.3

    #Not needed
    def read_from(self):
        return False

    def write_to(self, address, bit):
        match address:
            case 112:
                self.PCF_REGISTER[0] = bit
            case 114:
                self.PCF_REGISTER[1] = bit
            case 116:
                self.PCF_REGISTER[2] = bit
            case 118:
                self.PCF_REGISTER[3] = bit
            case 120:
                self.PCF_REGISTER[4] = bit
            case 122:
                self.PCF_REGISTER[5] = bit
            case 124:
                self.PCF_REGISTER[6] = bit
            case 126:
                self.PCF_REGISTER[7] = bit
            case _:
                raise ValueError('writing to non exising register address')

    def add_wood(self, wood, sensor):
        #print("wood added to sensor %d" % sensor)
        self.Sensors[sensor] = wood

    # Get measurement functionality
    def get_measurement(self):
        # Setting the sensor(wood) and register
        resistance = self.Resistor_Bank[self.get_resistance_number()]
        wood = self.Sensors[self.get_sensor_number()]

        # Calculate the measuring voltage
        if not isinstance(wood, type(None)):
            wood_resistance = wood.get_resistance()
            #print("wood resistance: {}".format(wood_resistance))
            #print("calculating current:")
            return self.power*(resistance/(resistance+wood_resistance))
        else:
            #print("Wood not connected")
            return self.power

    def get_resistance_number(self):
        return int(''.join(reversed(self.PCF_REGISTER[0:3])), 2)

    def get_sensor_number(self):
        return int(''.join(reversed(self.PCF_REGISTER[3:6])), 2)

    def set_sensor_moist(self, sensor, moist):
        if not (sensor in range(0, 8)):
            raise ValueError('set_sensor_moist sensor out of bounds')
        elif 0 >= moist or moist > 100:
            raise ValueError('set_sensor_moist moist level let than 0 or more than 100')
        self.Sensors[sensor].set_moist(moist)

    #Testing purposes only
    def get_sensor_moist(self, sensor):
        if not (sensor in range(0, 8)):
            raise ValueError('set_sensor_moist sensor out of bounds')
        return self.Sensors[sensor].get_moist()

    #Testing purposes only
    def get_unsertain_sensor_moist(self, sensor):
        if not (sensor in range(0, 8)):
            raise ValueError('set_sensor_moist sensor out of bounds')
        return self.Sensors[sensor].get_unsertain_moist()

#Simple runthroug testcase of data simulation
'''
test = Willow()
test.write_to(112, test.getBit(1))
#test.write_to(114, format(1, '01b'))
test.write_to(116, test.getBit(1))
print(test.get_resistance_number())

test.add_wood(Wood(-0.047, 1.079), 0)
print(test.get_measurement(14))
'''